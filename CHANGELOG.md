# Changelog

## 0.7.3

* added case-id header support for EMP-0100

## 0.7.2

* validated new fhir_questionnaire and fhir_questionnaire_response types as well as new selectors feature to support EMP-0105
* extended validation of preprocessing input types to support EMP-0105

## 0.7.1

* updated MDS for /fhir routes
* pinned compose service versions

## 0.7.0

* added pixelmapd and updated CDS
* updated definitions and dependencies

## 0.6.9

* updated definitions and dependencies

## 0.6.8

* updated definitions and dependencies

## 0.6.7

* fixed error in collection items iteration

## 0.6.6

* codestyle changes

## 0.6.5

* fixed validation of references to slide collections
* fixed class constraint validation for non leaf class values

## 0.6.4

* added validation for references into collections

## 0.6.3

* fixed class constraints within annotation collection issues

## 0.6.2

* fix class constraints and reference checks

## 0.6.1

* improved validation performance

## 0.6.0

* support for job validation

## 0.5.0

* support for added report mode

## 0.4.4

* validate config based on configuration section name

## 0.4.3

* implemented get_namespace_version
* updated definitions repo

## 0.4.2

* renamed ad-schema.v3 -> ad-schema.v3 at different occurences
* adapted to new changes in ead-schema.v3
  * namespace subversion

## 0.4.1

* validate containerized flag for modes
* use explicit encoding when opening files

## 0.4.0

* add EAD v2 validation support

## 0.3.0

* add config validation support
* restructured exceptions (EadValidationError and ConfigValidationError as bases)

## 0.2.0

* indepth validation support
* restructured package

## 0.1.0

* initial version with schema validation
